package com.company;

import java.io.IOException;

class Main
    {
        public static void main(String[] args) throws IOException {
            colorizing colorizing = new colorizing("grayImage.png");
            paint painting = new paint();
            painting.colorizing = colorizing;
            painting.main();
        }
    }
