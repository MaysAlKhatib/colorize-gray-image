package com.company;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

class colorPoint {
    Point p;
    javafx.scene.paint.Color color;

    public colorPoint(Point p, javafx.scene.paint.Color color) {
        this.p = p;
        this.color = color;
    }
}

public class paint extends Application {
    Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
    colorizing colorizing;
    Scene scene;
    ImageView imageView;
    ImageView imageView2;
    InputStream stream;
    InputStream stream2;
    ColorPicker colorPicker;
    Button button;
    Canvas canvas;
    Canvas canvas2;

    Image image;
    Image image2;
    Image image3;

    ArrayList<colorPoint> colorPoints = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws IOException {
        stream = new FileInputStream("grayImage.png");
        image = new Image(stream);
        imageView = new ImageView();
        imageView.setImage(image);
        imageView.setX(0);
        imageView.setY(0);
        imageView.setPreserveRatio(true);

        canvas = new Canvas(image.getWidth(), image.getHeight());
        canvas2 = new Canvas(image.getWidth(), image.getHeight());

        button = new Button();
        button.setText("Colorize image");
        button.setTranslateX(130);
        button.setFont(Font.font(15));
        button.setTranslateY(imageView.getFitHeight() - 25);

        final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        final GraphicsContext graphicsContext2 = canvas2.getGraphicsContext2D();
        initDraw(graphicsContext);
        initDraw(graphicsContext2);



        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        graphicsContext.beginPath();
                        graphicsContext.moveTo(event.getX(), event.getY());
                        colorPoints.add(new colorPoint(new Point((int) event.getX(), (int) event.getY()), colorPicker.getValue()));
                        graphicsContext.setStroke(colorPicker.getValue());
                        graphicsContext.stroke();
                    }
                });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        graphicsContext.lineTo(event.getX(), event.getY());
                        graphicsContext.setStroke(colorPicker.getValue());
                        graphicsContext.stroke();
                    }
                });

        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED,
                new EventHandler<MouseEvent>() {


                    @Override
                    public void handle(MouseEvent event) {

                    }
                });


        canvas2.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        graphicsContext2.beginPath();
                        graphicsContext2.moveTo(event.getX(), event.getY());
                        colorPoints.add(new colorPoint(new Point((int) event.getX(), (int) event.getY()), colorPicker.getValue()));
                        graphicsContext2.setStroke(colorPicker.getValue());
                        graphicsContext2.stroke();
                    }
                });

        canvas2.addEventHandler(MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        graphicsContext2.lineTo(event.getX(), event.getY());

                        graphicsContext2.setStroke(colorPicker.getValue());
                        graphicsContext2.stroke();
                    }
                });

        canvas2.addEventHandler(MouseEvent.MOUSE_RELEASED,
                new EventHandler<MouseEvent>() {


                    @Override
                    public void handle(MouseEvent event) {
                        int i = colorPoints.size() - 1;
                        try {
                            colorizing.floodfill(colorPoints.get(i).p.x, colorPoints.get(i).p.y,
                                    (int) (colorPoints.get(i).color.getRed() * 255), (int) (colorPoints.get(i).color.getGreen() * 255), (int) (colorPoints.get(i).color.getBlue() * 255));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        graphicsContext2.clearRect(0, 0, canvas2.getWidth(), canvas2.getHeight());

                        try {
                            stream2 = new FileInputStream("coloredImage.png");
                        } catch (FileNotFoundException ex) {
                            ex.printStackTrace();
                        }
                        image2 = new Image(stream2);

                        imageView2.setImage(image2);

                        Group root2 = new Group(imageView2);
                        VBox vBox = new VBox();
                        vBox.getChildren().addAll(canvas2, colorPicker);
                        root2.getChildren().add(vBox);

                        scene = new Scene(root2, screenSize.getWidth(), screenSize.getHeight() - 50);
                        primaryStage.setTitle("Colorize GrayScale Image");

                        primaryStage.setScene(scene);
                        primaryStage.show();

                    }
                });

        /////////////////////

        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                if (colorPoints.size() != 0) {
                    for (int i = 0; i < colorPoints.size(); i++) {
                        try {
                            colorizing.floodfill(colorPoints.get(i).p.x, colorPoints.get(i).p.y,
                                    (int) (colorPoints.get(i).color.getRed() * 255), (int) (colorPoints.get(i).color.getGreen() * 255), (int) (colorPoints.get(i).color.getBlue() * 255));

                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    }

                    try {
                        stream2 = new FileInputStream("coloredImage.png");
                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                    }
                    image3 = new Image(stream2);
                    imageView2 = new ImageView();
                    imageView2.setImage(image3);

                    Group root2 = new Group(imageView2);

                    VBox vBox = new VBox();
                    vBox.getChildren().addAll(canvas2, colorPicker);
                    root2.getChildren().add(vBox);

                    scene = new Scene(root2, screenSize.getWidth(), screenSize.getHeight() - 50);
                    primaryStage.setTitle("Colorize GrayScale Image");
                    primaryStage.setScene(scene);
                    primaryStage.show();


                }
            }
        };

        // when button is pressed
        button.setOnAction(event);



        Group root = new Group(imageView);
        VBox vBox = new VBox();
        vBox.getChildren().addAll(canvas, colorPicker, button);
        root.getChildren().add(vBox);
        scene = new Scene(root, screenSize.getWidth(), screenSize.getHeight() - 50);
        primaryStage.setTitle("Colorize GrayScale Image");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main() {
        launch();
    }

    private void initDraw(GraphicsContext gc) {

        colorPicker = new ColorPicker();

        double canvasWidth = gc.getCanvas().getWidth();
        double canvasHeight = gc.getCanvas().getHeight();

        gc.setStroke(Color.BLACK);
        gc.setLineWidth(5);

        gc.fill();
        gc.strokeRect(
                0,              //x of the upper left corner
                0,              //y of the upper left corner
                canvasWidth,    //width of the rectangle
                canvasHeight);  //height of the rectangle

        gc.setFill(colorPicker.getValue());
        gc.setStroke(colorPicker.getValue());
        gc.setLineWidth(3);
    }

}