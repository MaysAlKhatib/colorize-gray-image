package com.company;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
class colorizing{

    static File input;
    static BufferedImage screen;
    static BufferedImage screen1;
    public colorizing(String filename) throws IOException {
        input = new File(filename);
        screen = ImageIO.read(input);
        screen1 = ImageIO.read(input);
    }
    public static boolean isGoal(int x, int y, int target)
    {
        Color c2=new Color(screen.getRGB(x,y));
        int abs=Math.abs(c2.getRed()-target);
        return abs<=50;
    }

    public static <Queue> void floodfill(int x, int y, int r, int g, int b ) throws IOException {
        Color replacement=new Color(r,g,b);

        int  target = new Color(screen.getRGB(x,y)).getRed();

        boolean[][] painted = new boolean[screen.getWidth()][screen.getHeight()];
        for (int ii = 0; ii < screen.getWidth(); ii++) {
            for (int jj = 0; jj < screen.getHeight(); jj++) {

                painted[ii][jj] = false;
            }
        }


            java.util.Queue<Point> queue = new LinkedList<Point>();
            queue.add(new Point(x, y));

            while (!queue.isEmpty()) {
                Point p = queue.remove();

                if ((p.x >= 0)
                        && (p.x < screen.getWidth() && (p.y >= 0) && (p.y < screen
                        .getHeight()))) {
                    if (!painted[p.x][p.y]
                            && isGoal(p.x,p.y,target) ) {
                        painted[p.x][p.y] = true;
                        screen1.setRGB(p.x, p.y, replacement.getRGB());

                        queue.add(new Point(p.x + 1, p.y));
                        queue.add(new Point(p.x - 1, p.y));
                        queue.add(new Point(p.x, p.y + 1));
                        queue.add(new Point(p.x, p.y - 1));
                    }
                }
            }


        File ouptut = new File("coloredImage.png");
        ImageIO.write(screen1, "png", ouptut);
    }
}